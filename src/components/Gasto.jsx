import {
  LeadingActions,
  SwipeableList,
  SwipeableListItem,
  SwipeAction,
  TrailingActions
} from 'react-swipeable-list'
import "react-swipeable-list/dist/styles.css"

import { formatearFecha } from "../helpers"

import { 
    IconoSuscripciones, 
    IconoAhorro, 
    IconoCasa, 
    IconoComida, 
    IconoGastos, 
    IconoSalud, 
    IconoOcios 
} from "../assets/img/Imagenes"

const dictIcon = {
    ahorro : IconoAhorro,
    comida : IconoComida,
    casa : IconoCasa,
    gastos : IconoGastos,
    ocio : IconoOcios,
    salud : IconoSalud,
    suscripciones : IconoSuscripciones,
}

const Gasto = ({gasto, editGasto, deleteExpense}) => {
  
  const leadingActions = () => (
    <LeadingActions>
      <SwipeAction
        onClick={() => editGasto(gasto)}
      >
        Editar
      </SwipeAction>
    </LeadingActions>
  )

  const trailingActions = () => (
    <TrailingActions>
      <SwipeAction
        destructive={true}
        onClick={() => deleteExpense(gasto)}
      >
        Eliminar
      </SwipeAction>
    </TrailingActions>
  )
  return (
    <SwipeableList>
      <SwipeableListItem
        leadingActions={leadingActions()}
        trailingActions={trailingActions()}
      >
        <div className="gasto sombra">
          <div className="contenido-gasto">
            <img src={dictIcon[gasto.categoria]} alt="Icono Gasto" />
            <div className="descripcion-gasto">
                <p className="categoria">{gasto.categoria}</p>
                <p className="nombre-gasto">{gasto.nombre}</p>
                <p className="fecha-gasto">
                    Agregado el: {''}
                    <span>{formatearFecha(gasto.fecha)}</span>
                </p>
            </div>
          </div>
          <p className="cantidad-gasto">${gasto.monto}</p>
        </div>
      </SwipeableListItem>
    </SwipeableList>
  )
}

export default Gasto
