import Gasto from "./Gasto"

const ListadoGastos = ({
  gastos, 
  setEdit, 
  deleteExpense, 
  filtro, 
  gastosFiltrados
}) => {
  return (
    <div className="listado-gastos contenedor">
        
        {
          filtro
          ? (
              <>
                <h2>{ gastosFiltrados.length ? 'Gastos' : 'No hay gastos en estactegoría'}</h2>
                {
                  gastosFiltrados.map( elem => ( 
                    <Gasto 
                      gasto={elem} 
                      key={elem.id}
                      editGasto={gastos => setEdit(gastos)}
                      deleteExpense={value =>  deleteExpense(value)}
                    />
                  ))
                }
              </>
          )
          :(
            <> 
              <h2>{ gastos.length ? 'Gastos' : 'No hay gastos aún'}</h2>
              {
                gastos.map( elem => ( 
                  <Gasto 
                    gasto={elem} 
                    key={elem.id}
                    editGasto={gastos => setEdit(gastos)}
                    deleteExpense={value =>  deleteExpense(value)}
                  />
                ))
              }
            </> 
          )
        }
        
    </div>
  )
}

export default ListadoGastos
