import { useState, useEffect } from 'react'

import Header from './components/Header'
import Modal from './components/Modal'
import ListadoGastos from './components/ListadoGastos'

import AddExpense from './assets/img/nuevo-gasto.svg'
import Filtros from './components/Filtros'

function App() {

  const [presupuesto, setPresupuesto] = useState(Number(localStorage.getItem('presupuesto')) ?? 0)
  const [isValidPresupuesto, setIsValidPresupuesto] = useState(false)
  const [modal, setModal] = useState({
    view: false,
    animated: false,
    editGasto: {}
  })
  const [gastos, setGastos] = useState(JSON.parse(localStorage.getItem('gastos')) ?? [])
  const [filtro, setFiltro] = useState('')
  const [gastosFiltrados, setGastosFiltrados] = useState('')

  useEffect(() => {
    localStorage.setItem('gastos', JSON.stringify(gastos))
  }, [gastos])

  useEffect(() => {
    localStorage.setItem('presupuesto', presupuesto )
  }, [presupuesto])

  useEffect(() => {
    if(JSON.stringify(modal.editGasto) !== '{}'){
      HandleNewExpense()
    }
  }, [modal.editGasto])

  useEffect(() => {
    const gastosFiltrados = gastos.filter( elem => elem.categoria === filtro )
    setGastosFiltrados(gastosFiltrados)
  }, [filtro])

  useEffect(()=> {
    const presupuestoLS = Number(localStorage.getItem('presupuesto')) ?? 0
    if(presupuestoLS > 0){
      setIsValidPresupuesto(true)
    }
  }, [])

  const HandleNewExpense = () => {
    setModal({...modal, view: true})
    setTimeout(() => {
       setModal({...modal, view:true, animated: true})
    }, 500);
  }

  const saveExpense = (gasto) => {
    if(JSON.stringify(modal.editGasto) !== '{}'){
      const listaGastos = gastos.map( elem => elem.id != gasto.id ? elem : gasto)
      setGastos(listaGastos)
    }else{
      gasto.fecha = Date.now();
      setGastos([...gastos, gasto]);
    }
  }

  const deleteExpense = (gasto) => {
    // const close = confim('Desea eliminar este gasto?')
    // if(close){
      const listaGastos = gastos.filter( elem => elem.id != gasto.id)
      setGastos(listaGastos)
    //}
  }

  return (
    <div className={modal.view ? 'fijar' : ''}>
      <Header 
        gastos={gastos}
        setGastos={setGastos}
        presupuesto={presupuesto} 
        setPresupuesto={setPresupuesto}
        isValidPresupuesto={isValidPresupuesto}
        setIsValidPresupuesto={setIsValidPresupuesto}
      />
      
      {
        isValidPresupuesto &&
        <>
          <main>
            <Filtros 
              filtro={filtro}
              setFiltro={setFiltro}
            />
            <ListadoGastos 
              gastos={gastos}
              setEdit={(value) => setModal({...modal, editGasto: value})}
              deleteExpense={ item => deleteExpense(item)}
              filtro={filtro}
              gastosFiltrados={gastosFiltrados}
            />
          </main>
          <div className='nuevo-gasto'>
            <img 
              src={AddExpense} 
              alt="Icono nuevo gasto" 
              onClick={HandleNewExpense}
            />
          </div>
        </>
          
      }

      {
        modal.view && 
          <Modal 
            setModal={ value => setModal(value)} 
            modal={modal}
            save={ newItem => saveExpense(newItem)}
          />
      }
      
    </div>
  )
}

export default App
