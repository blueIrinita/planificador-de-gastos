export function generarId() {
    const fecha = Date.now().toString(36)
    const random  =  Math.random().toString(36).substr(2)
    return fecha + random
}

export const formatearFecha = fecha => {
    const  newDate = new Date(fecha);
    const opciones = {
        year:'numeric',
        month:'long',
        day: '2-digit'
    }
    return newDate.toLocaleDateString('es-ES', opciones)
}