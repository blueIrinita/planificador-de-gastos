import { useState, useEffect } from "react"
import Close from '../assets/img/cerrar.svg'
import { generarId } from "../helpers"
import Mensaje from "./Mensaje"


const Modal = ({setModal, modal, save}) => {

    const [gasto, setGasto] = useState( 
        JSON.stringify(modal.editGasto) !== '{}' 
        ? modal.editGasto
        : {
            nombre: '',
            monto: '',
            categoria: '',
            id: generarId()
        }
    )

    const [error, setError] = useState(false)

    const handleClose = () =>{
        setModal({animated:false, view:true, editGasto: {}})
        setTimeout(() => {
            setModal({animated:false, view:false, editGasto: {}})
         }, 500);
    }

    const handleSubmit = e  => {
        e.preventDefault()

        if(Object.values(gasto).some( x => x ==='')){
            setError(true)

            setTimeout(() => {
                setError(false)
            }, 2000);
            return
        }

        setError(false)
        save(gasto)
        handleClose()
    }

    return (
        <div className="modal">
            <div className="cerrar-modal">
                <img src={Close} onClick={handleClose}/>
            </div>
            <form 
                className={`formulario ${modal.animated ? 'animar' : 'cerrar' }`}
                onSubmit={handleSubmit}
            >
                <legend>{JSON.stringify(modal.editGasto) !== '{}' ? "Editar Gasto" : 'Nuevo Gasto'}</legend>
                { error && <Mensaje tipo={'error'}>Debes llenar todos los campos</Mensaje>}
                <div className="campo">
                    <label htmlFor="nombre">Nombre Gasto</label>
                    <input 
                        type="text" 
                        id="nombre" 
                        placeholder="Añade el Nombre del Gasto"
                        value={gasto.nombre}
                        onChange={ e  => setGasto({...gasto, nombre:e.target.value})}
                    />
                </div>
                <div className="campo">
                    <label htmlFor="cantidad">Monto</label>
                    <input 
                        type="number" 
                        id="cantidad" 
                        placeholder="Añadde la cantidad del gasto: ej.300"
                        value={gasto.monto}
                        onChange={ e => setGasto({...gasto, monto:Number(e.target.value)})}
                    />
                </div>
                <div className="campo">
                    <label htmlFor="categoria">Selecciona la categoria</label>
                    <select 
                        id="categoria"
                        value={gasto.categoria}
                        onChange={ e  => setGasto({...gasto, categoria:e.target.value})}
                    >
                        <option value="">-- Seleccione --</option>
                        <option value="ahorro">Ahorro</option>
                        <option value="comida">Comida</option>
                        <option value="casa">Casa</option>
                        <option value="gastos">Gastos Varios</option>
                        <option value="ocio">ocio</option>
                        <option value="salud">Salud</option>
                        <option value="suscripciones">Suscripciones</option>
                    </select>
                </div>
                <input type="submit" value={JSON.stringify(modal.editGasto) !== '{}' ? "Modificar Gasto" :"Añadir Gasto"} />
            </form>
            
        </div>
    )
}

export default Modal
