import { useState } from "react";
import Mensaje from "./Mensaje";

const NuevoPresupuesto = ({presupuesto, setPresupuesto, setIsValidPresupuesto}) => {

    const [mensaje, setMensaje] = useState(false)

    const HandlerPresupuesto = e => {
        e.preventDefault()
        if(!presupuesto || presupuesto < 0){
            setMensaje('No esun presupuesto valido');
            return
        }

        setMensaje('')
        setIsValidPresupuesto(true)
    }

  return (
    <div className='contenedor-presupuesto contenedor sombra'>
      <form className='formulario' onSubmit={HandlerPresupuesto}>
        <div className='campo'>
            <label>Definir Presupuesto</label>
            <input 
                type={'number'}
                placeholder={"Añade tu presupuesto"}
                className={"nuevo-presupuesto"}
                value={presupuesto}
                onChange={ e => setPresupuesto(Number(e.target.value)) }
            />
        </div>
        <input type={'submit'} value="Añadir" />
        {mensaje && <Mensaje tipo={'error'}>{mensaje}</Mensaje> }
      </form>
    </div>
  )
}

export default NuevoPresupuesto
