import IconoAhorro from './icono_ahorro.svg'
import IconoCasa from './icono_casa.svg'
import IconoComida from  './icono_comida.svg'
import IconoGastos from './icono_gastos.svg'
import IconoOcios from './icono_ocio.svg'
import IconoSalud from './icono_salud.svg'
import IconoSuscripciones from './icono_suscripciones.svg'

export {
    IconoAhorro,
    IconoCasa,
    IconoComida,
    IconoGastos,
    IconoOcios,
    IconoSalud,
    IconoSuscripciones,
}
